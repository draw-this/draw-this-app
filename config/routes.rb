Rails.application.routes.draw do
  root to: "home#index"

  resources :descriptions, only: [:index, :show, :new, :create] do
    collection do
      get :request_call
      post :submit_request_call
      get :success_request_call
    end

    member do
      get :success_upload
      get :upload_drawing
    end
  end
  resources :drawings, only: [:show, :create, :index]
  get :drawers, to: "home#drawers", as: :drawers
  get :storytellers, to: "home#storytellers", as: :storytellers
  get :about, to: "home#about", as: :about
  get :privacy, to: "home#privacy", as: :privacy

  namespace :admin do
    resources :descriptions
    resources :describers
    resources :drawings

    root to: "describers#index"
  end
end
