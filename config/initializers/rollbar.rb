if Rails.env.production? || Rails.env.staging?
  Rollbar.configure do |config|
    config.access_token = ENV["ROLLBAR_ACCESS_TOKEN"]
    config.environment = Rails.env

    config.exception_level_filters.merge!(
      "ActionController::RoutingError" => "ignore",
    )
  end
end
