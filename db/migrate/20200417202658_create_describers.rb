class CreateDescribers < ActiveRecord::Migration[6.0]
  def change
    create_table :describers do |t|
      t.string :name, null: false
      t.string :email
      t.string :address
      t.string :phone_number
      t.timestamps
    end
  end
end
