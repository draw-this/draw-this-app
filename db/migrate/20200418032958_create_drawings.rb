class CreateDrawings < ActiveRecord::Migration[6.0]
  def change
    create_table :drawings do |t|
      t.string :author, null: false
      t.references :description
      t.timestamps
    end
  end
end
