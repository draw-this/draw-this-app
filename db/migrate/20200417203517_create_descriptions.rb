class CreateDescriptions < ActiveRecord::Migration[6.0]
  def change
    create_table :descriptions do |t|
      t.string :title, null: false
      t.references :describer
      t.timestamps
    end
  end
end
