class AddEmailToDrawings < ActiveRecord::Migration[6.0]
  def change
    change_table(:drawings) do |t|
      t.column :email, :string
    end
  end
end
