class AddApprovedToDescriptions < ActiveRecord::Migration[6.0]
  def change
    change_table(:descriptions) do |t|
      t.column :approved, :bool, null: false, default: false
    end
    Description.update_all(approved: true)
  end
end
