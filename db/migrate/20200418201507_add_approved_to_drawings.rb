class AddApprovedToDrawings < ActiveRecord::Migration[6.0]
  def change
    change_table(:drawings) do |t|
      t.column :approved, :bool, null: false, default: false
    end
  end
end
