// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

require("@rails/ujs").start();
require("turbolinks").start();
require("@rails/activestorage").start();

// Uncomment to copy all static images under ../images to the output folder and reference
// them with the image_pack_tag helper in views (e.g <%= image_pack_tag 'rails.png' %>)
// or the `imagePath` JavaScript helper below.
//
// const images = require.context('../images', true)
// const imagePath = (name) => images(name, true)

document.addEventListener("turbolinks:load", function() {
  handleNav();
  handlePlayback();
  handleUploads();

  const rForm = document.getElementById("recaptcha-form");
  if (!rForm) return;

  rForm.addEventListener("submit", e => {
    e.preventDefault();
    grecaptcha.execute();
  });
});

function handleNav() {
  const nav = document.getElementById("nav");
  if (!nav) return;

  document.addEventListener("scroll", e => {
    const offset = window.pageYOffset;
    if (offset >= 100) {
      nav.classList.add("folded");
    } else {
      nav.classList.remove("folded");
    }
  });
}

function handlePlayback() {
  const player = document.getElementById("description-audio");
  if (!player) return;

  const passed = document.getElementById("player-passed");
  const left = document.getElementById("player-left");
  const seeker = document.getElementById("player-seeker");
  const btnPlay = document.getElementById("btn-play");
  const btnPause = document.getElementById("btn-pause");
  const btnRetry = document.getElementById("btn-retry");
  let seeking = false;

  const stringify = num => (num >= 10 ? num.toString() : `0${num}`);
  const formatTime = sec =>
    `${stringify(Math.floor(sec / 60))}:${stringify(Math.floor(sec % 60))}`;

  btnPlay.onclick = () => {
    player.play();
  };
  btnPause.onclick = () => {
    player.pause();
  };
  btnRetry.onclick = () => {
    player.pause();
    player.currentTime = 0;
    player.play();
  };
  player.addEventListener("timeupdate", e => {
    passed.innerHTML = formatTime(player.currentTime);
    left.innerHTML = formatTime(player.duration);
    seeker.max = player.duration;
    if (!seeking) seeker.value = player.currentTime;
  });
  seeker.addEventListener("mousedown", () => {
    seeking = true;
  });
  seeker.addEventListener("change", e => {
    player.fastSeek(seeker.value);
  });
  seeker.addEventListener("mouseup", () => {
    seeking = false;
  });
}

function handleUploads() {
  const input = document.getElementById("file-input");
  if (!input) return;

  const label = document.getElementById("filename-label");
  input.addEventListener("change", f => {
    if (input.files.length < 1) return;

    const file = input.files[0];
    label.innerHTML = file.name;

    const img = document.getElementById("selected-image");
    if (!img) return;

    const reader = new FileReader();
    reader.onload = ({ target }) => {
      img.src = target.result;
      img.style.display = "initial";
    };
    reader.readAsDataURL(file);
  });
}

window.submitInvisibleRecaptchaForm = () => {
  document.getElementById("recaptcha-form").submit();
};
