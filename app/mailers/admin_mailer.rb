class AdminMailer < ApplicationMailer
  default to: 'hello@drawthis.nz'

  def request_call
    @call_request = params[:call_request]
    mail(subject: 'New call request')
  end
end
