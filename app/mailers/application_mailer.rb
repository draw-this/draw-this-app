class ApplicationMailer < ActionMailer::Base
  default from: "hello@drawthis.nz"
  layout "mailer"
end
