class Drawing < ApplicationRecord
  belongs_to :description
  has_one_attached :image

  validates :author, presence: true
  validates :image, presence: true
  validates :email, format: { with: URI::MailTo::EMAIL_REGEXP }, unless: -> { email.blank? }
  validate :image_type

  scope :approved, -> { where(approved: true) }

  private

  def image_type
    return unless image.attached?
    return if image.blob.content_type.starts_with?('image/')

    errors.add(:image, 'has the wrong format')
  end
end
