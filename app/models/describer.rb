class Describer < ApplicationRecord
  has_many :descriptions, dependent: :destroy

  validates :name, presence: true
  validates :email, presence: true, unless: :address?
  validates :address, presence: true, unless: :email?

  def street_address
    address_components[0]
  end

  def suburb
    address_components[1]
  end

  def city
    address_components[2]
  end

  def postcode
    address_components[3]
  end

  private

  def address_components
    address&.split || []
  end
end
