class Description < ApplicationRecord
  belongs_to :describer
  has_many :drawings, dependent: :destroy
  has_one_attached :recording

  validates :title, presence: true
  validates :recording, presence: true
  validate :recording_type

  delegate :name, to: :describer, prefix: true

  accepts_nested_attributes_for :describer

  scope :approved, -> { where(approved: true) }

  def full_title
    "#{describer.name}'s #{title}"
  end

  private

  def recording_type
    return unless recording.attached?
    return if recording.blob.content_type.starts_with?('audio/')

    errors.add(:recording, 'has the wrong format')
  end
end
