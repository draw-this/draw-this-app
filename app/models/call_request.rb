class CallRequest
  include ActiveModel::Validations

  attr_accessor :name, :email, :phone, :comment

  validates :name, :phone, presence: true

  def initialize(params = {})
    @name = params[:name]
    @phone = params[:phone]
    @email = params[:email]
    @comment = params[:comment]
  end
end
