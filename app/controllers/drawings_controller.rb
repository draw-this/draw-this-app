class DrawingsController < ApplicationController
  def index
    @drawings = Drawing.approved
  end

  def show
    @drawing = Drawing.find(params[:id])
  end

  def create
    @description = Description.find(params[:description_id])
    @drawing = Drawing.new(drawing_params)
    @drawing.description = @description
    if verify_privacy_policy && verify_recaptcha(model: @drawing) && @drawing.save
      redirect_to(drawing_path(@drawing))
    else
      render("descriptions/upload_drawing")
    end
  end

  private

  def verify_privacy_policy
    valid = params[:privacy] == "1"
    @drawing.errors.add(:base, "You have to agree to the privacy policy") unless valid
    valid
  end

  def drawing_params
    params.require(:drawing).permit(:author, :image, :email)
  end
end
