class DescriptionsController < ApplicationController
  def index
    @descriptions = Description.approved
  end

  def show
    @description = Description.approved.find(params[:id])
    @drawings = @description.drawings.approved
  end

  def upload_drawing
    @description = Description.approved.find(params[:id])
  end

  def new
    @description = Description.new(describer: Describer.new)
  end

  def create
    @description = Description.new(description_params)
    if verify_privacy_policy && verify_recaptcha(model: @description) && @description.save
      redirect_to(success_upload_description_path(@description))
    else
      render(:new)
    end
  end

  def request_call
    @call_request = CallRequest.new
  end

  def submit_request_call
    @call_request = CallRequest.new(call_request_params)
    if verify_recaptcha(model: @call_request) && @call_request.valid?
      AdminMailer.with(call_request: @call_request).request_call.deliver_now
      redirect_to(success_request_call_descriptions_path)
    else
      render(:request_call)
    end
  end

  private

  def verify_privacy_policy
    valid = params[:privacy] == "1"
    @description.errors.add(:base, "You have to agree to the privacy policy") unless valid
    valid
  end

  def description_params
    all_params = params.require(:description)
    address_params = all_params
                       .require(:describer_attributes)
                       .extract!(:street_address, :suburb, :city, :postcode)

    all_params
      .permit(:title, :recording, describer_attributes: [:name, :email])
      .tap { |params| params[:describer_attributes].merge!(address: address_params.values.join) }
  end

  def describer_address
    params
      .require(:description)
      .require(:describer_attributes)
      .extract!(:street_address, :suburb, :city, :postcode)
      .values
      .join
  end

  def call_request_params
    params.require(:call_request).permit(:name, :email, :phone, :comment)
  end
end
